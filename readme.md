# CLONET FLIX

Criando interface gráfica inspirada no NETFLIX usando apenas HTML, CSS e Javascript.

## Visualizando a aplicação

Para visualizar este pequeno experimento em execução:

1. `docker-compose up -d`
2. `http://127.0.0.1:8080`