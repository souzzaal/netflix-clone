$('document').ready(function () {
	let atracaoId = Math.floor(Math.random() * (db.length - 1 + 1)) + 1;
	destacar(atracaoId)
	inicializarCarrossel()
	$('.box-filme').click(evento => {
		// evento.preventDefault()
		destacar(evento.target.id)
	})

	$("#fecharModal").click(e => {
		$('#trailer')[0].src = $('#trailer')[0].src
	})

});

function destacar(id) {
	const atracao = db.find(atr => atr.id == id)
	$('.em-destaque h3.titulo').html(atracao.titulo)
	$('.em-destaque p.descricao').html(atracao.descricao)

	$("#trailer")[0].src = atracao.trailer

	document
		.querySelector('.em-destaque')
		.style
		.background = `linear-gradient(rgba(0,0,0,.50), rgba(0,0,0,.50)), url('./img/${atracao.imagem_destaque}')`
}

function inicializarCarrossel() {
	const carrossel = $('#carousel')[0]
	db.forEach(atracao => {
		let div = document.createElement('div')
		div.className = "item"
		let img = document.createElement('img')
		img.className = "box-filme"
		img.src = `/img/${atracao.imagem_capa}`
		img.id = atracao.id
		div.appendChild(img)
		carrossel.appendChild(div)
	})

	$('.owl-carousel').owlCarousel({
		loop: true,
		margin: 10,
		nav: true,
		responsive: {
			0: {
				items: 1
			},
			600: {
				items: 3
			},
			1000: {
				items: 5
			}
		}
	})
}
