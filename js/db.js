const db = [
	{
		"id": 1,
		"titulo": "UM MALUCO NO PEDAÇO",
		"descricao": "A vida na mansão da poderosa família Banks vira de cabeça para baixo quando Will, um parente malandro da Filadélfia, vai morar com eles na mansão de Bel-Air.",
		"imagem_capa": "capa1.jpg",
		"imagem_destaque": "destaque1.jpg",
		"trailer": "https://www.youtube.com/embed/LeW6VX8Wrf8"
	},
	{
		"id": 2,
		"titulo": "Flash",
		"descricao": "Barry enfrenta diversos assassinos que invadem a premiação onde ele acompanha Iris, mas os efeitos colaterais de seus novos superpoderes representam uma ameaça.",
		"imagem_capa": "capa2.jpg",
		"imagem_destaque": "destaque2.jpg",
		"trailer": "https://www.youtube.com/embed/N-QSZKxkBjo"
	},
	{
		"id": 3,
		"titulo": "La Casa de Papel",
		"descricao": "O Professor e sua equipe se reúnem para libertar Rio com um plano ousado e perigoso. Desta vez, o alvo é o Banco da Espanha. A Resistência continua.",
		"imagem_capa": "capa3.jpg",
		"imagem_destaque": "destaque3.jpg",
		"trailer": "https://www.youtube.com/embed/yKXYXgo7a3I"
	},
	{
		"id": 4,
		"titulo": "Os 100",
		"descricao": "Quase 100 anos após um apocalipse nuclear devastar a Terra, 100 sobreviventes de uma estação espacial voltam para avaliar as condições do planeta.",
		"imagem_capa": "capa4.jpg",
		"imagem_destaque": "destaque4.jpg",
		"trailer": "https://www.youtube.com/embed/aDrsItJ_HU4"
	},
	{
		"id": 5,
		"titulo": "Arqueiro",
		"descricao": "Inspirada nos quadrinhos do Arqueiro Verde, esta série acompanha as aventuras do playboy que se torna super-herói e luta contra vilões armado apenas com arco e flechas.",
		"imagem_capa": "capa5.jpg",
		"imagem_destaque": "destaque5.jpg",
		"trailer": "https://www.youtube.com/embed/2yrviapP5uY"

	},
	{
		"id": 6,
		"titulo": "The rain",
		"descricao": "Seis anos após um vírus exterminar quase toda a população da Escandinávia, dois irmãos e um grupo de jovens sobreviventes partem em busca de segurança e de respostas.",
		"imagem_capa": "capa6.jpg",
		"imagem_destaque": "destaque6.jpg",
		"trailer": "https://www.youtube.com/embed/Se3AfHO7GmY"
	}
]